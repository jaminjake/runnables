package com.evans;
/**
 * Main.java
 * Course: CIT260-8
 * Name: Jake Evans
 */
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) {
        // write your code here
        ExecutorService myService = Executors.newFixedThreadPool(3);
        SendOrderThread order1 = new SendOrderThread(1,"Jake", "Evans", 4805551212L);
        SendOrderRunnable order2 = new SendOrderRunnable(2, "Bill", "Smith", 6025551212L);
        SendOrderThread order3 = new SendOrderThread(3, "Joseph", "Smith", 9235551212L);
        SendOrderRunnable order4 = new SendOrderRunnable(4, "Brigham", "Young", 2085551212L);
        SendOrderThread order5 = new SendOrderThread(5, "Steve", "Young", 5095551212L);

        myService.execute(order1);
        myService.execute(order2);
        myService.execute(order3);
        myService.execute(order4);
        myService.execute(order5);

        myService.shutdown();
    }
}