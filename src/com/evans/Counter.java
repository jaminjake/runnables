package com.evans;
/**
 * Counter.java
 * Course: CIT260-8
 * Name: Jake Evans
 * https://www.codejava.net/java-core/concurrency/understanding-atomic-variables-in-java
 */

import java.util.concurrent.atomic.AtomicInteger;

public class Counter {
    private AtomicInteger counter = new AtomicInteger();

    public void increment() {
        counter.incrementAndGet();
    }

    public void decrement() {
        counter.decrementAndGet();
    }

    public int get() {
        return counter.get();
    }
}
