package com.evans;

import java.util.Random;

/**
 * SendOrderThread.java
 * Course: CIT260-8
 * Name: Jake Evans
 */

public class SendOrderThread extends Thread {
    static final int threads = 2;
    private int orderNumber;
    private String firstName;
    private String lastName;
    private long phoneNumber;
    private int rand;
    private int counter;


    public SendOrderThread(int orderNumber, String firstName, String lastName, long phoneNumber) {
        this.orderNumber = orderNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        Random random = new Random();
        this.rand = random.nextInt(100);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public void run() {
        System.out.println("\nSending Order Number: " + orderNumber + ", Name: " + firstName + " " + lastName +
                ", Phone Number: " + phoneNumber);
        for (counter = 0; counter < threads; counter++) {
            System.out.println("\nTrying to send Order Number: " + orderNumber + ", Name: " + firstName + " " + lastName +
                    ", Phone Number: " + phoneNumber);
            try {
                Thread.sleep(rand);
            } catch (InterruptedException e) {
                System.err.println(e.toString());
            }
        }
        System.out.println("\nFinished Sending Order Number: " + orderNumber + ", Name: " + firstName + " " + lastName +
                ", Phone Number: " + phoneNumber);
    }
}
